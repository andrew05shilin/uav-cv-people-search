from imageai.Detection import ObjectDetection
import os

exec_path = os.getcwd()

detector = ObjectDetection()
detector.setModelTypeAsRetinaNet()
detector.setModelPath(os.path.join(
	exec_path, "resnet50_coco_best_v2.1.0.h5")
)
detector.loadModel()

custom = detector.CustomObjects(person=True, dog=True)



list = detector.detectCustomObjectsFromImage(
	custom_objects=custom,
	input_image=os.path.join(exec_path, "objects.jpg"),
	output_image_path=os.path.join(exec_path, "photos.jpg"),
	minimum_percentage_probability=20,
	display_percentage_probability=True,
	display_object_name=True
	
)  





